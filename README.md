Check out the main thread here: http://forum.zdoom.org/viewtopic.php?f=19&t=48985
=================================================================================

This GIT is the current development branch of Faspons. What you will see here is 
a bunch of giberish that is supposed to work to bring you a DOOM / DOOM 2 mod
that should be alright.

You can either download the mod here on GitLab or on the ZDOOM thread. Once
downloaded, you can do the following:

    1. Extract the ZIP file
    2. Run _zip.BAT
    3. Type either pk3 or pk7
    4. Once completed, close the CMD prompt and run the compiled file
	
If you have any questions, bug reports, feature requests, etc.; post in the 
main thread or add an issue/feature request here in GitLab.

Change log: https://gitlab.com/faslrn/Faspons/blob/release/CHANGELOG
=================================================================================