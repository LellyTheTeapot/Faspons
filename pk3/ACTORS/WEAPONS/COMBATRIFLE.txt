ACTOR CombatRifle : Weapon 
{
	Weapon.SelectionOrder 500
	Weapon.AmmoType1 "RifleAmmo"
	Weapon.AmmoGive1 20
	Weapon.AmmoUse1 1
	Weapon.AmmoType2 "Magazine"
	Weapon.AmmoUse2 1
	Weapon.UpSound "weapons/riotgun/raise"
	Weapon.BobStyle "Smooth"
	Weapon.BobSpeed 3.0
	Weapon.BobRangeX 0.25
	Weapon.BobRangeY 0.50
	Inventory.Pickupmessage "You got the Combat Rifle!"
	Inventory.PickupSound "weapons/rifle/up"
	Obituary "%o was gunned down by %k's Rifle."
	+WEAPON.NOALERT
	+WEAPON.AMMO_OPTIONAL
	Tag "Combat Rifle"
	Scale 1.0
	States
	{
		Ready:
			TNT1 A 0 A_TakeInventory("SlidePlayer", 1)
			TNT1 A 0 A_JumpIfInventory("ReloadingEnabled", 1, "ReadyReload")
			Goto RegReady
		RegReady:
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			ASLT A 1 A_WeaponReady
			Loop
		ReadyReload:
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			TNT1 A 0 A_JumpIfInventory("Magazine",0,2)
			TNT1 A 0 A_JumpIfInventory("RifleAmmo",1,2)
			ASLT A 1 A_WeaponReady
			Loop
			ASLT A 1 A_WeaponReady
			ASLT A 0 A_JumpIfInventory("IsReloading", 1, "Reload")
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			Loop
		Deselect:
			ASLT A 1 Offset(2, 34)
			ASLT A 1 Offset(7, 39)
			ASLR A 1 Offset(10, 47)
			ASLR A 1 Offset(22, 58)
			ASLR A 1 Offset(32, 69)
			ASLR A 1 Offset(54, 81)
			ASLR A 1 Offset(67, 100)
			ASLR A 1 A_Lower
			Wait
		Select: 
			ASLR A 1 A_Raise
			ASLR A 1 Offset(67, 100)
			ASLR A 1 Offset(54, 81)
			ASLR A 1 Offset(32, 69)
			ASLR A 1 Offset(22, 58)
			ASLR A 1 Offset(10, 47)
			ASLT A 1 Offset(7, 39)
			ASLT A 1 Offset(2, 34)
			Goto Ready
		Fire:
			TNT1 A 0 A_JumpIfInventory("ReloadingEnabled", 1, "FireReload")
			Goto FireReg
		FireReg:
			TNT1 A 0 A_JumpIfNoAmmo("NoAmmo")
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			TNT1 AAAA 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("RifleAmmo",1,TIF_NOTAKEINFINITE)
			ASLT A 0 A_AlertMonsters
			ASLT A 0 A_PlaySound("weapons/rifle/fire", CHAN_WEAPON)
			ASLT A 0 A_FireBullets(0,0,-1,6,"AltPuff",0)
			TNT1 A 0 A_FireCustomMissile("Tracer", 0, 0, 0, -18, 0, 0)
			TNT1 A 0 A_ZoomFactor(0.995)
			TNT1 A 0 A_SetPitch(pitch-0.25)
			CRFI A 1 BRIGHT A_Light1 
			TNT1 A 0 A_ZoomFactor(0.99)
			ASLT A 0 A_SetPitch(pitch+0.25)
			CRFI B 1 BRIGHT A_Light2
			TNT1 A 0 A_ZoomFactor(0.995)
			TNT1 A 0 A_Light0
			TNT1 A 0 A_FireCustomMissile("MuzzSpawn",0,0,0,0)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 2)
			TNT1 A 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,0)
			TNT1 A 0 A_FireCustomMissile("RifleCasingSpawner",5,0,8,-9)
			ASLT B 1 Offset( 0, 34 ) A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(1.00)
			ASLT C 1 Offset( 0, 33 ) A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASLT A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("HoldReg")
			Goto Ready
		HoldReg:
			TNT1 A 0 A_JumpIfNoAmmo("NoAmmo")
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			TNT1 AAAA 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("RifleAmmo",1,TIF_NOTAKEINFINITE)
			ASLT A 0 A_AlertMonsters
			ASLT A 0 A_PlaySound("weapons/rifle/fire", CHAN_WEAPON)
			TNT1 A 0 A_FireCustomMissile("Tracer", random(0,1), 0, 0, -18, 0, random(-1,1))
			ASLT A 0 A_FireBullets(1,2,-1,6,"AltPuff",0)
			TNT1 A 0 A_ZoomFactor(0.995)
			TNT1 A 0 A_SetPitch(pitch-0.25)
			CRFI A 1 BRIGHT A_Light1 
			TNT1 A 0 A_ZoomFactor(0.99)
			ASLT A 0 A_SetPitch(pitch+0.25)
			CRFI B 1 BRIGHT A_Light2
			TNT1 A 0 A_ZoomFactor(0.995)
			TNT1 A 0 A_Light0
			TNT1 A 0 A_FireCustomMissile("MuzzSpawn",0,0,0,0)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 2)
			TNT1 A 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,0)
			TNT1 A 0 A_FireCustomMissile("RifleCasingSpawner",5,0,8,-9)
			ASLT B 1 Offset( 0, 34 ) A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(1.00)
			ASLT C 1 Offset( 0, 33 ) A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASLT A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("HoldReg")
			Goto Ready
		FireReload:
			TNT1 A 0 A_JumpIfInventory("Magazine",1,2)
			TNT1 A 0 A_GiveInventory("RifleEmpty", 1)
			Goto Reload
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			TNT1 AAAA 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("Magazine",1,TIF_NOTAKEINFINITE)
			ASLT A 0 A_AlertMonsters
			ASLT A 0 A_PlaySound("weapons/rifle/fire", CHAN_WEAPON)
			ASLT A 0 A_FireBullets(0,0,-1,5,"AltPuff",0)
			TNT1 A 0 A_FireCustomMissile("Tracer", 0, 0, 0, -18, 0, 0)
			TNT1 A 0 A_ZoomFactor(0.995)
			CRFI A 1 BRIGHT A_SetPitch(pitch-0.25)
			TNT1 A 0 A_Light1
			TNT1 A 0 A_ZoomFactor(0.99)
			ASLT A 0 A_SetPitch(pitch+0.25)
			CRFI B 1 BRIGHT A_Light2
			TNT1 A 0 A_ZoomFactor(0.995)
			TNT1 A 0 A_Light0
			TNT1 A 0 A_FireCustomMissile("MuzzSpawn",0,0,0,0)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 2)
			TNT1 A 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,0)
			TNT1 A 0 A_FireCustomMissile("RifleCasingSpawner",5,0,8,-9)
			ASLT B 1 Offset( 0, 34 ) A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(1.00)
			ASLT C 1 Offset( 0, 33 ) A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASLT A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("HoldReload")
			TNT1 A 0 A_JumpIfInventory("Magazine",1,2)
			TNT1 A 0 A_GiveInventory("RifleEmpty", 1)
			TNT1 A 0
			Goto Ready
		HoldReload:
			TNT1 A 0 A_JumpIfInventory("Magazine",1,2)
			TNT1 A 0 A_GiveInventory("RifleEmpty", 1)
			Goto Reload
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			TNT1 AAAA 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("Magazine",1,TIF_NOTAKEINFINITE)
			ASLT A 0 A_AlertMonsters
			ASLT A 0 A_PlaySound("weapons/rifle/fire", CHAN_WEAPON)
			ASLT A 0 A_FireBullets(1,2,-1,5,"AltPuff",0)
			TNT1 A 0 A_FireCustomMissile("Tracer", random(0,1), 0, 0, -18, 0, random(-1,1))
			TNT1 A 0 A_ZoomFactor(0.995)
			TNT1 A 0 A_SetPitch(pitch-0.25)
			CRFI A 1 BRIGHT A_Light1 
			TNT1 A 0 A_ZoomFactor(0.99)
			ASLT A 0 A_SetPitch(pitch+0.25)
			CRFI B 1 BRIGHT A_Light2
			TNT1 A 0 A_ZoomFactor(0.995)
			TNT1 A 0 A_Light0
			TNT1 A 0 A_FireCustomMissile("MuzzSpawn",0,0,0,0)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 2)
			TNT1 A 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,0)
			TNT1 A 0 A_FireCustomMissile("RifleCasingSpawner",5,0,8,-9)
			ASLT B 1 Offset( 0, 34 ) A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(1.00)
			ASLT C 1 Offset( 0, 33 ) A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASLT A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("HoldReload")
			TNT1 A 0 A_JumpIfInventory("Magazine",1,2)
			TNT1 A 0 A_GiveInventory("RifleEmpty", 1)
			TNT1 A 0
			Goto Ready
		Reload:
			TNT1 A 0 A_JumpIfNoAmmo("NoAmmo")
			ASLR ABCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_PlaySound("weapons/rifle/out")
			TNT1 A 0 A_PlayWeaponSound("weapons/rifle/in")
			ASLR L 1 Offset( -2, 33 )
			ASLR L 1 Offset( -3, 34 )
			TNT1 A 0 A_FireCustomMissile("RifleMagDrop",-5,0,0,-10)
			ASLR K 1 Offset( -5, 34 )
			ASLR K 1 Offset( -2, 33 )
			ASLR GG 4 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto Reloadloop
		ReloadLoop:
			TNT1 A 0 A_GiveInventory("Magazine")
			TNT1 A 0 A_TakeInventory("RifleAmmo",1,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIfInventory("Magazine",30,"ReloadEnd")
			TNT1 A 0 A_JumpIfInventory("RifleAmmo",1,"ReloadLoop")
			Goto ReloadEnd
		ReloadEnd:
			ASLR HIJK 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASLR L 1 Offset( 2, 33 )
			ASLR N 2 Offset( 8, 34 )
			ASLR N 3 Offset( 10, 35 )
			ASLR N 1 Offset( 5, 33 )
			ASLR O 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASLR Q 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASLR S 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASLT A 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_JumpIfInventory("RifleEmpty", 1, "PullBolt")
			Goto ReadyReload
		PullBolt:
			ASER ABC 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_PlaySound("PULLBOLT", 7)
			ASER D 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASER E 5 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASER D 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASER CBA 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			ASLT A 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("RifleEmpty", 1)
			Goto Ready
		DoKick:
			TNT1 A 0 A_JumpIfInventory("BerserkPower", 1, "DoBerserkKick")
			TNT1 A 0 A_Takeinventory("Kicking",1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			KKCR ABCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			KKCR E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			KKCR F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			KKCR G 1 A_PlaySound("KICK", 0)
			TNT1 A 0 A_ZoomFactor(0.96)
			TNT1 A 0 A_FireCustomMissile("KickAttack", 0, 0, 0, 0)
			TNT1 A 0 A_SetPitch(-1.5 + pitch)
			KKCR H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-0.5 + pitch)
			KKCR H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(+1.0 + pitch)
			KKCR H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(+1.0 + pitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			KKCR G 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			KKCR F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			KKCR E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(1.00)
			KKCR DCBA 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 SetPlayerProperty(0,0,0)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady
			Goto Ready
		DoBerserkKick:
			TNT1 A 0 A_Takeinventory("Kicking",1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			KKCR ABCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			KKCR E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			KKCR F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			KKCR G 1 A_PlaySound("KICK", 0)
			TNT1 A 0 A_ZoomFactor(0.96)
			TNT1 A 0 A_FireCustomMissile("BerserkKickAttack", 0, 0, 0, 0)
			TNT1 A 0 A_SetPitch(-1.5 + pitch)
			KKCR H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-0.5 + pitch)
			KKCR H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(+1.0 + pitch)
			KKCR H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(+1.0 + pitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			KKCR G 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			KKCR F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			KKCR E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(1.00)
			KKCR DCBA 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 SetPlayerProperty(0,0,0)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady
			Goto Ready
		ThrowGrenade:
			TNT1 A 0
			TNT1 A 0 A_TakeInventory("GrenadeToss",1)
			TNT1 A 0 A_JumpIfInventory("HandGrenadeAmmo",1,1)
			Goto Ready
			TNT1 A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS ABCCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSD D 1 A_PlaySound("GPIN")
			TOSS EF 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 8 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TNT1 A 0 A_Takeinventory("HandGrenadeAmmo",1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS H 1 A_FireCustomMissile("johnnybomb",0,false,7,3,true,6)
			TNT1 A 0 A_SetPitch(+2 + pitch)
			TOSS I 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS J 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS K 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS L 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 9 A_Takeinventory("GrenadeToss",1)
			TNT1 A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 1 A_WeaponReady
			Goto Ready
		NoAmmo:
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_PlaySound("bulletup", CHAN_ITEM)
			ASLT AAAAAAAAAAAAAAAAAAA 2 A_WeaponReady(WRF_NoFire)
			Goto Ready
		TossingAmmo:
			SAWN A 0 A_Takeinventory("TossAmmo",1)
			SAWN A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
		TMag:
			SAWN A 0 A_JumpIfInventory("TossMag", 0, 2) //Toss mag
			SAWN A 0 A_Jump(256, "TShells") //Jump if you do not have "TossMag" selected
			SAWN A 0 A_JumpIfInventory("RifleAmmo", 30, 1)
			Goto CantToss
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO )
			TOSS H 1 A_FireCustomMissile("TossedMag",0,false,7,3,true,3)
			TNT1 A 0 A_TakeInventory("RifleAmmo", 30)
			Goto TEnd
		TShells:
			SAWN A 0 A_JumpIfInventory("TossShells", 0, 2) //Toss shells
			SAWN A 0 A_Jump(256, "TNato") //Jump if you do not have "Toss Shells" selected
			TNT1 A 0 A_JumpIfInventory("Shell", 4, 1)
			Goto CantToss
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TOSS H 1 A_FireCustomMissile("TossedShells",0,false,7,3,true,3)
			TNT1 A 0 A_TakeInventory("Shell", 4)
			Goto TEnd
		TNato:
			SAWN A 0 A_JumpIfInventory("TossNato", 0, 2) //Toss NATO
			SAWN A 0 A_Jump(256, "TRockets") //Jump if you do not have "Toss Nato" selected
			TNT1 A 0 A_JumpIfInventory("Nato", 50, 1)
			Goto CantToss
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TOSS H 1 A_FireCustomMissile("TossedNato",0,false,7,3,true,3)
			TNT1 A 0 A_TakeInventory("Nato", 50)
			Goto TEnd
		TRockets:
			SAWN A 0 A_JumpIfInventory("TossRockets", 0, 2) //Toss rockets
			SAWN A 0 A_Jump(256, "TCell") //Jump if you do not have "Toss Rockets" selected
			TNT1 A 0 A_JumpIfInventory("RocketAmmo", 5, 1)
			Goto CantToss
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TOSS H 1 A_FireCustomMissile("TossedRockets",0,false,7,3,true,3)
			TNT1 A 0 A_TakeInventory("RocketAmmo", 5)
			Goto TEnd
		TCell:
			SAWN A 0 A_JumpIfInventory("TossCell", 0, 2) //Toss cell
			SAWN A 0 A_Jump(256, "TEnd") //Jump if you do not have "TossMag" selected
			TNT1 A 0 A_JumpIfInventory("Cell", 40, 1)
			Goto CantToss
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TOSS H 1 A_FireCustomMissile("TossedCell",0,false,7,3,true,3)
			TNT1 A 0 A_TakeInventory("Cell", 40)
			Goto TEnd
		TEnd:
			TNT1 A 0 A_SetPitch(+2 + pitch)
			TOSS I 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS J 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS K 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS L 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 1 A_WeaponReady
			Goto Ready
		CantToss:
			ASLT A 0 A_PlaySound("Nope", 4)
			Goto Ready
		IsSliding:
			TNT1 A 0 A_JumpIfInventory("CrouchPlayer", 1, "Ready")
			TNT1 A 0 A_PlaySound("SlideKick/Start",4)
			TNT1 A 0 A_PlaySound("SlideKick/Loop",5,0.5,TRUE)
			TNT1 A 0 A_SetPitch(pitch+0.8)
			SLDK ABCDEF 2 
			TNT1 A 0 A_SetPitch(pitch-0.8)
			SLDK G 6 A_FireCustomMissile("KickAttack", 0, 0, 0, -13)
			TNT1 A 0 A_StopSound(5)
			TNT1 A 0 A_PlaySound("SlideKick/End",6)
			SLDK HI 2
			Goto Ready
		Spawn:
			RIFL A -1
			Stop
	}
}

ACTOR Magazine : Ammo
{
	+IGNORESKILL
	Inventory.MaxAmount 30
}

ACTOR RifleEmpty : Inventory {
	Inventory.MaxAmount 1
}