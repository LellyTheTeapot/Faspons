ACTOR AmmoSpawner : RandomSpawner replaces Clip
{
	DropItem RifleAmmo
}

ACTOR Ammobox : RandomSpawner replaces Clipbox
{
	DropItem Nato
}

ACTOR RifleAmmo : Ammo 
{
	Inventory.Amount 15
	Inventory.MaxAmount 150
	Ammo.BackpackAmount 45
	Ammo.BackpackMaxAmount 300
	Inventory.PickupMessage "Collected a rifle magazine."
	Inventory.PickupSound "weapons/rifle/slap"
	States
	{
		Spawn:
			CLIP A -1
			Stop
	}
}

ACTOR Nato : Ammo
{
	Inventory.Icon "BELSA0"
	Inventory.PickupSound "ammoboxup"
	Inventory.Amount 50
	Inventory.MaxAmount 250
	Ammo.BackpackAmount 50
	Ammo.BackpackMaxAmount 500
	Scale 0.5
	States
	{
		Spawn:
			BELT A -1
			Stop
	}
}

ACTOR RocketClip : CustomInventory 
{
	Inventory.PickupMessage "Picked up a rocket clip."
	Inventory.PickupSound "weapons/rifle/slap"
	States
	{
		Spawn:
			RCSE A -1
			Stop
		Pickup:
			TNT1 A 0 A_GiveInventory ("RocketAmmo", 5)
	}
}


ACTOR Shells : Shell replaces Shell
{
	Inventory.PickupSound "SHELLPU"
}

ACTOR BoxofShells : Shellbox replaces Shellbox
{
	Inventory.PickupSound "SHELLPU"
}

ACTOR Cells : Cell Replaces Cell
{
	Inventory.PickupSound "CPAKUP"
}

ACTOR CellPacks : CellPack Replaces CellPack
{
	Inventory.PickupSound "CPAKUP"
}