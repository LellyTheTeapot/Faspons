ACTOR BFG : Weapon replaces BFG9000
{
	Weapon.SelectionOrder 2800
	Weapon.AmmoType1 "Cell"
	Weapon.AmmoGive1 40
	Weapon.AmmoUse1 1
	Weapon.AmmoType2 "BFGCell"
	Weapon.AmmoUse2 1
	Weapon.UpSound "weapons/bfg/pickup"
	Weapon.BobStyle "Smooth"
	Weapon.BobSpeed 3.0
	Weapon.BobRangeX 0.25
	Weapon.BobRangeY 0.50
	+WEAPON.AMMO_OPTIONAL
	Obituary "%o was turned inside out by %k's BFG"
	Inventory.PickupMessage "$GOTBFG9000"
	Inventory.PickupSound "weapons/bfg/pickup"
	Tag "$TAG_BFG9000"
	States
	{
		Ready:
			TNT1 A 0 A_TakeInventory("SlidePlayer", 1)
			TNT1 A 0 A_JumpIfInventory("ReloadingEnabled", 1, "ReadyReload")
			Goto RegReady
		RegReady:
			TNT1 A 0 A_TakeInventory("BFGStage1", 1)
			TNT1 A 0 A_TakeInventory("BFGStage2", 1)
			TNT1 A 0 A_TakeInventory("BFGStage3", 1)
			TNT1 A 0 A_TakeInventory("BFGStage4", 1)
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			DBFG A 1 A_WeaponReady
			Loop
		ReadyReload:
			TNT1 A 0 A_TakeInventory("BFGStage1", 1)
			TNT1 A 0 A_TakeInventory("BFGStage2", 1)
			TNT1 A 0 A_TakeInventory("BFGStage3", 1)
			TNT1 A 0 A_TakeInventory("BFGStage4", 1)
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			TNT1 A 0 A_JumpIfInventory("BFGCell",0,2)
			TNT1 A 0 A_JumpIfInventory("Cell",1,2)
			DBFG A 1 A_WeaponReady
			Loop
			DBFG A 1 A_WeaponReady
			DBFG A 0 A_JumpIfInventory("IsReloading", 1, "Reload")
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			Loop	
		Deselect:
			DBFG A 1 Offset(2, 34)
			DBFG A 1 Offset(7, 39)
			DBFG A 1 Offset(10, 47)
			DBFG A 1 Offset(22, 58)
			DBFG A 1 Offset(32, 69)
			DBFG A 1 Offset(54, 81)
			DBFG A 1 Offset(67, 100)
			DBFG A 1 A_Lower
			Wait
		Select:
			DBFG A 1 A_Raise
			DBFG A 1 Offset(67, 100)
			DBFG A 1 Offset(54, 81)
			DBFG A 1 Offset(32, 69)
			DBFG A 1 Offset(22, 58)
			DBFG A 1 Offset(10, 47)
			DBFG A 1 Offset(7, 39)
			DBFG A 1 Offset(2, 34)
			Goto Ready
		Fire:
			TNT1 A 0 A_JumpIfInventory("ReloadingEnabled", 1, "FireReload")
			Goto FireReg
		FireReg:
			TNT1 A 0 A_JumpIfNoAmmo("NoAmmo")
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			TNT1 A 0 A_PlaySound("weapons/bfg/classic", CHAN_WEAPON)
			TNT1 A 0 A_PlaySound("weapons/bfg/start", 2)
			TNT1 A 0 A_GiveInventory("BFGStage1", 1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_Light1
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireRegLoad1")
			Goto HyperPowerReg
		FireRegLoad1:
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireRegLoad2")
			Goto HyperPowerReg
		FireRegLoad2:
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireRegLoad3")
			Goto HyperPowerReg
		FireRegLoad3:
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireRegLoad4")
			Goto HyperPowerReg
		FireRegLoad4:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireRegLoad5")
			Goto HyperPowerReg
		FireRegLoad5:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireRegLoad6")
			Goto HyperPowerReg
		FireRegLoad6:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireRegLoad7")
			Goto HyperPowerReg
		FireRegLoad7:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldReg")
			Goto HyperPowerReg
		FireReload:
			TNT1 A 0 A_JumpIfInventory("BFGCell",1,1)
			Goto Reload
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_JumpIfInventory("Kicking",1,"DoKick")
			TNT1 A 0 A_JumpIfInventory("SlidePlayer",1,"IsSliding")
			TNT1 A 0 A_PlaySound("weapons/bfg/classic", CHAN_WEAPON)
			TNT1 A 0 A_PlaySound("weapons/bfg/start", 2)
			TNT1 A 0 A_GiveInventory("BFGStage1", 1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_Light1
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireReloadLoad1")
			Goto HyperPower
		FireReloadLoad1:
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireReloadLoad2")
			Goto HyperPower
		FireReloadLoad2:
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireReloadLoad3")
			Goto HyperPower
		FireReloadLoad3:
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireReloadLoad4")
			Goto HyperPower
		FireReloadLoad4:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireReloadLoad5")
			Goto HyperPower
		FireReloadLoad5:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireReloadLoad6")
			Goto HyperPower
		FireReloadLoad6:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireReloadLoad7")
			Goto HyperPower
		FireReloadLoad7:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldReload")
			Goto HyperPower
		HoldReload:
			TNT1 A 0 A_JumpIfInventory("BFGCell",26,1)
			Goto HyperPower
			TNT1 A 0 A_TakeInventory("BFGStage1", 1)
			TNT1 A 0 A_GiveInventory("BFGStage2", 1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldReloadLoad1")
			Goto HyperPower
		HoldReloadLoad1:
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldReloadLoad2")
			Goto HyperPower
		HoldReloadLoad2:
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldReloadLoad3")
			Goto HyperPower
		HoldReloadLoad3:
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldReloadLoad4")
			Goto HyperPower
		HoldReloadLoad4:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldReloadLoad5")
			Goto HyperPower
		HoldReloadLoad5:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldReloadLoad6")
			Goto HyperPower
		HoldReloadLoad6:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldReloadLoad7")
			Goto HyperPower
		HoldReloadLoad7:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3")
			Goto HyperPower
		FireStage3:
			TNT1 A 0 A_JumpIfInventory("BFGCell",1,1)
			Goto Reload
			TNT1 A 0 A_JumpIfInventory("BFGCell",31,1)
			Goto HyperPower
			TNT1 A 0 A_TakeInventory("BFGStage2", 1)
			TNT1 A 0 A_GiveInventory("BFGStage3", 1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_Light2
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load1")
			Goto HyperPower
		FireStage3Load1:
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load2")
			Goto HyperPower
		FireStage3Load2:
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load3")
			Goto HyperPower
		FireStage3Load3:
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load4")
			Goto HyperPower
		FireStage3Load4:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load5")
			Goto HyperPower
		FireStage3Load5:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load6")
			Goto HyperPower
		FireStage3Load6:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load7")
			Goto HyperPower
		FireStage3Load7:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireCustomMissile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage4")
			Goto HyperPower
		FireStage4:
			TNT1 A 0 A_JumpIfInventory("BFGCell",1,1)
			Goto Reload
			TNT1 A 0 A_JumpIfInventory("BFGCell",36,1)
			Goto HyperPower
			TNT1 A 0 A_TakeInventory("BFGStage3", 1)
			TNT1 A 0 A_GiveInventory("BFGStage4", 1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC GHIJ 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto HyperPower
		HyperPower:
			TNT1 A 0 A_JumpIfInventory("BFGStage1",0,"Power1")
			TNT1 A 0 A_JumpIfInventory("BFGStage2",0,"Power2")
			TNT1 A 0 A_JumpIfInventory("BFGStage3",0,"Power3")
			TNT1 A 0 A_JumpIfInventory("BFGStage4",0,"Power4")
			Goto Ready
		Power1:
			TNT1 A 0 A_TakeInventory("BFGStage1", 1)
			TNT1 A 0 A_StopSound(1)
			TNT1 A 0 A_StopSound(2)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.985)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("BFGCell",25,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 7)
			TNT1 AAAAAA 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 Offset( 0, 34 )
			DBFF B 1 BRIGHT A_FireCustomMissile("HyperNova",0,0,0,0)
			TNT1 A 0 A_Recoil(2)
			TNT1 A 0 Offset( 0, 33 ) A_ZoomFactor(0.99)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 ) A_ZoomFactor(0.995)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 ) A_ZoomFactor(1.00)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 )
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		Power2:
			TNT1 A 0 A_TakeInventory("BFGStage2", 1)
			TNT1 A 0 A_StopSound(1)
			TNT1 A 0 A_StopSound(2)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.9875)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("BFGCell",30,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 7)
			TNT1 AAAAAA 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 Offset( 0, 36 )
			DBFF B 1 BRIGHT A_FireCustomMissile("HyperNova2",0,0,0,0)
			TNT1 A 0 A_Recoil(3)
			TNT1 A 0 Offset( 0, 35 ) A_ZoomFactor(0.9875)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 34 ) A_ZoomFactor(0.995)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 33 ) A_ZoomFactor(1.00)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 )
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		Power3:
			TNT1 A 0 A_TakeInventory("BFGStage3", 1)
			TNT1 A 0 A_StopSound(1)
			TNT1 A 0 A_StopSound(2)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.985)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.975)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("BFGCell",35,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 7)
			TNT1 AAAAAA 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 Offset( 0, 38 )
			DBFF B 1 BRIGHT A_FireCustomMissile("HyperNova3",0,0,0,0)
			TNT1 A 0 A_Recoil(4)
			TNT1 A 0 Offset( 0, 36 ) A_ZoomFactor(0.985)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 34 ) A_ZoomFactor(0.995)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 33 ) A_ZoomFactor(1.00)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 )
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		Power4:
			TNT1 A 0 A_TakeInventory("BFGStage4", 1)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.965)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("BFGCell",40,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 7)
			TNT1 AAAAAA 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 Offset( 0, 40 )
			DBFF B 1 BRIGHT A_FireCustomMissile("HyperNova4",0,0,0,0)
			TNT1 A 0 A_Recoil(5)
			TNT1 A 0 Offset( 0, 38 ) A_ZoomFactor(0.98)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 36 ) A_ZoomFactor(0.995)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 34 ) A_ZoomFactor(1.00)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 )
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_PlaySound("weapons/plasmagun/beep",7)
			DBFG ABABBB 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		HoldReg:
			TNT1 A 0 A_JumpIfInventory("Cell",26,1)
			Goto HyperPowerReg
			TNT1 A 0 A_TakeInventory("BFGStage1", 1)
			TNT1 A 0 A_GiveInventory("BFGStage2", 1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("HoldRegLoad1")
			Goto HyperPowerReg
		HoldRegLoad1:
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("HoldRegLoad2")
			Goto HyperPowerReg
		HoldRegLoad2:
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("HoldRegLoad3")
			Goto HyperPowerReg
		HoldRegLoad3:
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("HoldRegLoad4")
			Goto HyperPowerReg
		HoldRegLoad4:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("HoldRegLoad5")
			Goto HyperPowerReg
		HoldRegLoad5:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("HoldRegLoad6")
			Goto HyperPowerReg
		HoldRegLoad6:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("HoldRegLoad7")
			Goto HyperPowerReg
		HoldRegLoad7:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("FireStage3Reg")
			Goto HyperPowerReg
		FireStage3Reg:
			TNT1 A 0 A_JumpIfInventory("Cell",31,1)
			Goto HyperPowerReg
			TNT1 A 0 A_TakeInventory("BFGStage2", 1)
			TNT1 A 0 A_GiveInventory("BFGStage3", 1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_Light2
			TNT1 A 0 A_ReFire("FireStage3RegLoad1")
			Goto HyperPowerReg
		FireStage3RegLoad1:
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("FireStage3RegLoad2")
			Goto HyperPowerReg
		FireStage3RegLoad2:
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("FireStage3RegLoad3")
			Goto HyperPowerReg
		FireStage3RegLoad3:
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("FireStage3RegLoad4")
			Goto HyperPowerReg
		FireStage3RegLoad4:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("FireStage3RegLoad5")
			Goto HyperPowerReg
		FireStage3RegLoad5:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("FireStage3RegLoad6")
			Goto HyperPowerReg
		FireStage3RegLoad6:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("FireStage3RegLoad7")
			Goto HyperPowerReg
		FireStage3RegLoad7:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ReFire("FireStage4Reg")
			Goto HyperPowerReg
		FireStage4Reg:
			TNT1 A 0 A_JumpIfInventory("Cell",36,1)
			Goto HyperPower
			TNT1 A 0 A_TakeInventory("BFGStage3", 1)
			TNT1 A 0 A_GiveInventory("BFGStage4", 1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC GHIJ 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto HyperPowerReg
		HyperPowerReg:
			TNT1 A 0 A_JumpIfInventory("BFGStage1",0,"Power1Reg")
			TNT1 A 0 A_JumpIfInventory("BFGStage2",0,"Power2Reg")
			TNT1 A 0 A_JumpIfInventory("BFGStage3",0,"Power3Reg")
			TNT1 A 0 A_JumpIfInventory("BFGStage4",0,"Power4Reg")
			Goto Ready
		Power1Reg:
			TNT1 A 0 A_TakeInventory("BFGStage1", 1)
			TNT1 A 0 A_StopSound(1)
			TNT1 A 0 A_StopSound(2)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.985)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("Cell",25,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 7)
			TNT1 AAAAAA 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 Offset( 0, 34 )
			DBFF B 1 BRIGHT A_FireCustomMissile("HyperNova",0,0,0,0)
			TNT1 A 0 A_Recoil(2)
			TNT1 A 0 Offset( 0, 33 ) A_ZoomFactor(0.99)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 ) A_ZoomFactor(0.995)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 ) A_ZoomFactor(1.00)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 )
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		Power2Reg:
			TNT1 A 0 A_TakeInventory("BFGStage2", 1)
			TNT1 A 0 A_StopSound(1)
			TNT1 A 0 A_StopSound(2)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.9875)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("Cell",30,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 7)
			TNT1 AAAAAA 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 Offset( 0, 36 )
			DBFF B 1 BRIGHT A_FireCustomMissile("HyperNova2",0,0,0,0)
			TNT1 A 0 A_Recoil(3)
			TNT1 A 0 Offset( 0, 35 ) A_ZoomFactor(0.9875)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 34 ) A_ZoomFactor(0.995)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 33 ) A_ZoomFactor(1.00)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 )
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		Power3Reg:
			TNT1 A 0 A_TakeInventory("BFGStage3", 1)
			TNT1 A 0 A_StopSound(1)
			TNT1 A 0 A_StopSound(2)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.985)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.975)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("Cell",35,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 7)
			TNT1 AAAAAA 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 Offset( 0, 38 )
			DBFF B 1 BRIGHT A_FireCustomMissile("HyperNova3",0,0,0,0)
			TNT1 A 0 A_Recoil(4)
			TNT1 A 0 Offset( 0, 36 ) A_ZoomFactor(0.985)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 34 ) A_ZoomFactor(0.995)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 33 ) A_ZoomFactor(1.00)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 )
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		Power4Reg:
			TNT1 A 0 A_TakeInventory("BFGStage4", 1)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.965)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("Cell",40,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 7)
			TNT1 AAAAAA 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 Offset( 0, 40 )
			DBFF B 1 BRIGHT A_FireCustomMissile("HyperNova4",0,0,0,0)
			TNT1 A 0 A_Recoil(5)
			TNT1 A 0 Offset( 0, 38 ) A_ZoomFactor(0.98)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 36 ) A_ZoomFactor(0.995)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 34 ) A_ZoomFactor(1.00)
			DBFF B 1 BRIGHT 
			TNT1 A 0 Offset( 0, 32 )
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_PlaySound("weapons/plasmagun/beep",7)
			DBFG ABABBB 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		Reload:
			TNT1 A 0 A_JumpIfNoAmmo("NoAmmo")
			TNT1 A 0 A_PlaySound ("weapons/bfg/reload", CHAN_AUTO)
			TNT1 A 0 A_FireCustomMissile("PulseCellDrop",-5,0,4,-10)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 2)
			TNT1 A 0 A_FireCustomMissile("GunSmokeSpawner",0,0,0,-5)
			BFGR A 20 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			BFGR BCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto ReloadLoop
		ReloadLoop:
			TNT1 A 0 A_GiveInventory("BFGCell")
			TNT1 A 0 A_TakeInventory("Cell",1,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIfInventory("BFGCell",0,"ReloadEnd")
			TNT1 A 0 A_JumpIfInventory("Cell",1,"ReloadLoop")
			Goto ReloadEnd
		ReloadEnd:
			BFGR E 1
			BFGR GH 3
			BFGR I 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			BFGR JK 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			BFGR LMNOPQ 5 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFG A 5 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		DoKick:
			TNT1 A 0 A_JumpIfInventory("BerserkPower", 1, "DoBerserkKick")
			TNT1 A 0 A_Takeinventory("Kicking",1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			KKBG ABCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			KKBG E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			KKBG F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			KKBG G 1 A_PlaySound("KICK", 0)
			TNT1 A 0 A_ZoomFactor(0.96)
			TNT1 A 0 A_FireCustomMissile("KickAttack", 0, 0, 0, 0)
			TNT1 A 0 A_SetPitch(-1.5 + pitch)
			KKBG H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-0.5 + pitch)
			KKBG H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(+1.0 + pitch)
			KKBG H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(+1.0 + pitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			KKBG G 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			KKBG F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			KKBG E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(1.00)
			KKBG DCBA 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 SetPlayerProperty(0,0,0)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady
			Goto Ready
			Goto Ready
		DoBerserkKick:
			TNT1 A 0 A_Takeinventory("Kicking",1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			KKBG ABCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			KKBG E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			KKBG F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			KKBG G 1 A_PlaySound("KICK", 0)
			TNT1 A 0 A_ZoomFactor(0.96)
			TNT1 A 0 A_FireCustomMissile("BerserkKickAttack", 0, 0, 0, 0)
			TNT1 A 0 A_SetPitch(-1.5 + pitch)
			KKBG H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-0.5 + pitch)
			KKBG H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(+1.0 + pitch)
			KKBG H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(+1.0 + pitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			KKBG G 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			KKBG F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			KKBG E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(1.00)
			KKBG DCBA 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 SetPlayerProperty(0,0,0)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady
			Goto Ready
		ThrowGrenade:
			TNT1 A 0
			TNT1 A 0 A_TakeInventory("GrenadeToss",1)
			TNT1 A 0 A_JumpIfInventory("HandGrenadeAmmo",1,1)
			Goto Ready
			TNT1 A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS ABCCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSD D 1 A_PlaySound("GPIN")
			TOSS EF 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 8 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TNT1 A 0 A_Takeinventory("HandGrenadeAmmo",1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS H 1 A_FireCustomMissile("johnnybomb",0,false,7,3,true,6)
			TNT1 A 0 A_SetPitch(+2 + pitch)
			TOSS I 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS J 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS K 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS L 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 9 A_Takeinventory("GrenadeToss",1)
			TNT1 A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 1 A_WeaponReady
			Goto Ready
		NoAmmo:
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_PlaySound("weapons/plasmagun/beep",0)
			DBFG AAAAAAAAAAAAAAAAAAA 2 A_WeaponReady(WRF_NoFire)
			Goto Ready
		TossingAmmo:
			SAWN A 0 A_Takeinventory("TossAmmo",1)
			SAWN A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
		TMag:
			SAWN A 0 A_JumpIfInventory("TossMag", 0, 2) //Toss mag
			SAWN A 0 A_Jump(256, "TShells") //Jump if you do not have "TossMag" selected
			SAWN A 0 A_JumpIfInventory("RifleAmmo", 30, 1)
			Goto CantToss
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TOSS H 1 A_FireCustomMissile("TossedMag",0,false,7,3,true,3)
			TNT1 A 0 A_TakeInventory("RifleAmmo", 30)
			Goto TEnd
		TShells:
			SAWN A 0 A_JumpIfInventory("TossShells", 0, 2) //Toss shells
			SAWN A 0 A_Jump(256, "TNato") //Jump if you do not have "Toss Shells" selected
			TNT1 A 0 A_JumpIfInventory("Shell", 4, 1)
			Goto CantToss
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TOSS H 1 A_FireCustomMissile("TossedShells",0,false,7,3,true,3)
			TNT1 A 0 A_TakeInventory("Shell", 4)
			Goto TEnd
		TNato:
			SAWN A 0 A_JumpIfInventory("TossNato", 0, 2) //Toss NATO
			SAWN A 0 A_Jump(256, "TRockets") //Jump if you do not have "Toss Nato" selected
			TNT1 A 0 A_JumpIfInventory("Nato", 50, 1)
			Goto CantToss
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TOSS H 1 A_FireCustomMissile("TossedNato",0,false,7,3,true,3)
			TNT1 A 0 A_TakeInventory("Nato", 50)
			Goto TEnd
		TRockets:
			SAWN A 0 A_JumpIfInventory("TossRockets", 0, 2) //Toss rockets
			SAWN A 0 A_Jump(256, "TCell") //Jump if you do not have "Toss Rockets" selected
			TNT1 A 0 A_JumpIfInventory("RocketAmmo", 6, 1)
			Goto CantToss
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TOSS H 1 A_FireCustomMissile("TossedRockets",0,false,7,3,true,3)
			TNT1 A 0 A_TakeInventory("RocketAmmo", 6)
			Goto TEnd
		TCell:
			SAWN A 0 A_JumpIfInventory("TossCell", 0, 2) //Toss cell
			SAWN A 0 A_Jump(256, "TEnd") //Jump if you do not have "TossMag" selected
			TNT1 A 0 A_JumpIfInventory("Cell", 40, 1)
			Goto CantToss
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TOSS H 1 A_FireCustomMissile("TossedCell",0,false,7,3,true,3)
			TNT1 A 0 A_TakeInventory("Cell", 40)
			Goto TEnd
		TEnd:
			TNT1 A 0 A_SetPitch(+2 + pitch)
			TOSS I 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS J 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS K 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS L 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 1 A_WeaponReady
			Goto Ready
		CantToss:
			DBFG A 0 A_PlaySound("Nope", 4)
			Goto Ready
		IsSliding:
			TNT1 A 0 A_JumpIfInventory("CrouchPlayer", 1, "Ready")
			TNT1 A 0 A_PlaySound("SlideKick/Start",4)
			TNT1 A 0 A_PlaySound("SlideKick/Loop",5,0.5,TRUE)
			TNT1 A 0 A_SetPitch(pitch+0.8)
			SLDK ABCDEF 2 
			TNT1 A 0 A_SetPitch(pitch-0.8)
			SLDK G 6 A_FireCustomMissile("KickAttack", 0, 0, 0, -13)
			TNT1 A 0 A_StopSound(5)
			TNT1 A 0 A_PlaySound("SlideKick/End",6)
			SLDK HI 2
			Goto Ready
		Spawn:
			DEBF A -1
			Stop
	}
}

ACTOR HyperNova : BFGBall
{
	Decal "BFGLightning"
	Radius 14
	Height 9
	Speed 90
	Alpha 0.95
	Scale 0.1
	Damage 40
	DamageType Plasma
	Translation "192:207=112:127"
	DeathSound "none"
	States
	{
		Spawn:
			BBAL ABCD 4 BRIGHT
			Loop
		Death:
			TNT1 A 0 A_CustomMissile ("Residue", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0 A_BFGSpray ("BFGExtra", 25, 5)
			BBB1 ABCDE 4 Bright
			TNT1 A 0 A_PlaySound ("weapons/bfgx")
			RR00 ABCDEF 3 Bright
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 2)
			TNT1 A 0 A_CustomMissile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0
			Stop
	}
}

ACTOR HyperNova2 : BFGBall
{
	Decal "BFGLightning"
	Radius 14
	Height 9
	Speed 85
	Alpha 0.95
	Scale 0.2
	Damage 60
	DamageType Plasma
	Translation "192:207=112:127"
	DeathSound "none"
	States
	{
		Spawn:
			BBAL ABCD 4 BRIGHT
			Loop
		Death:
			TNT1 A 0 A_CustomMissile ("Residue", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0 A_BFGSpray ("BFGExtra", 30, 10)
			BBB1 ABCDE 4 Bright
			TNT1 A 0 A_PlaySound ("weapons/bfgx")
			RR00 ABCDEF 3 Bright
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 2)
			TNT1 A 0 A_CustomMissile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0
			Stop
	}
}

ACTOR HyperNova3 : BFGBall
{
	Decal "BFGLightning"
	Radius 14
	Height 9
	Speed 80
	Alpha 0.95
	Scale 0.3
	Damage 80
	DamageType Plasma
	Translation "192:207=112:127"
	DeathSound "none"
	States
	{
		Spawn:
			BBAL ABCD 4 BRIGHT
			Loop
		Death:
			TNT1 A 0 A_CustomMissile ("Residue", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0 A_BFGSpray ("BFGExtra", 35, 15)
			BBB1 ABCDE 4 Bright
			TNT1 A 0 A_PlaySound ("weapons/bfgx")
			RR00 ABCDEF 3 Bright
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 2)
			TNT1 A 0 A_CustomMissile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0
			Stop
	}
}

ACTOR HyperNova4 : BFGBall
{
	Decal "BFGLightning"
	Radius 14
	Height 9
	Speed 75
	Alpha 0.95
	Scale 0.6
	Damage 100
	DamageType Plasma
	Translation "192:207=112:127"
	DeathSound "none"
	States
	{
		Spawn:
			BBAL ABCD 4 BRIGHT
			Loop
		Death:
			TNT1 A 0 A_CustomMissile ("Residue", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0 A_BFGSpray ("BFGExtra", 40, 20)
			BBB1 ABCDE 4 Bright
			TNT1 A 0 A_PlaySound ("weapons/bfgx")
			RR00 ABCDEF 3 Bright
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 2)
			TNT1 A 0 A_CustomMissile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0
			Stop
	}
}

ACTOR Nova : BFGBall
{
	Decal "Scorch"
	Radius 7
	Height 9
	Speed 50
	Alpha 0.95
	Scale 0.8
	Damage 20
	DamageType Plasma
	Translation "192:207=112:127"
	SeeSound	"none"
	DeathSound "weapons/plasmad/explode"
	States
	{
		Spawn:
			BBB1 E 4 BRIGHT
			Loop
		Death:
			TNT1 A 0 A_BFGSpray ("BFGExtra", 10, 2)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 2)
			TNT1 A 0 A_CustomMissile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			VFX3 ABCD 2 Bright
			VFX3 EFGH 3 Bright
			VFX3 IJ 3
			Stop
	}
}

ACTOR Residue : BFGExtra
{
	+NOBLOCKMAP
	+NOGRAVITY
	RenderStyle Add
	Alpha 0.25
	Scale 0.4
	Damage 0
	DamageType "BFGSplash"
	States
	{
		Spawn:
			TNT1 A 0 A_BFGSpray ("BFGExtra", 5, 5)
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, 2)
			TNT1 A 0 A_CustomMissile ("RocketSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			R044 JKLMNOPQJKLMNOPQJKLMNOPQJKLMNOPQ 5 A_FadeOut(0.02)
			Stop
	}
}

ACTOR BFGCell : Ammo
{
	+IGNORESKILL
	Inventory.MaxAmount 160
}