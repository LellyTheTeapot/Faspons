Actor ArchvileRemix : Archvile Replaces Archvile
{
	Health 700
	GibHealth 20
	Radius 20
	Height 56
	Mass 500
	Speed 15
	PainChance 5
	PainChance "SuperBuckshot", 7
	PainChance "Saw", 10
	Monster
	MaxTargetRange 896
	+QUICKTORETALIATE
	+FLOORCLIP
	+NOTARGET
	-FRIGHTENED
	SeeSound "vile/sight"
	PainSound "vile/pain"
	DeathSound "vile/death"
	ActiveSound "vile/active"
	MeleeSound "vile/stop"
	Obituary "$OB_VILE"
	States
	{
	Spawn:
		VILE AB 10 A_Look
		Loop
	See:
		TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(913)==1, "SeeVanilla")
		VILE AABBCCDDEEFF 2 A_VileChase
		Loop
	SeeVanilla:
		TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(913)==0, "See")
		VILE AABBCCDDEEFF 2 A_Chase(0, "MissileVanilla", CHF_RESURRECT)
		Loop
	MissileVanilla:
		VILE G 0 Bright A_VileStart
		VILE G 10 Bright A_FaceTarget
		VILE H 8 Bright A_VileTarget
		VILE IJKLMN 8 Bright A_FaceTarget
		VILE O 8 Bright A_VileAttack
		VILE P 20 Bright
		Goto See
	Missile:
		VILE G 0 A_FaceTarget
		TNT1 A 0 A_JumpIfCloser(250, "SecMissile")
		VILE G 0 Bright A_VileStart
		VILE G 10 Bright A_FaceTarget
	Stage2:
		TNT1 A 0 A_JumpIfCloser(250, "SecMissile2")
		VILE H 8 Bright A_VileTarget
	Stage3:
		TNT1 A 0 A_JumpIfCloser(250, "SecMissile3")
		VILE I 8 Bright A_FaceTarget
	Stage4:
		TNT1 A 0 A_JumpIfCloser(250, "SecMissile4")
		VILE J 8 Bright A_FaceTarget
	Stage5:
		TNT1 A 0 A_JumpIfCloser(250, "SecMissile5")
		VILE K 8 Bright A_FaceTarget
	Stage6:
		TNT1 A 0 A_JumpIfCloser(250, "SecMissile6")
		VILE L 8 Bright A_FaceTarget
	Stage7:
		TNT1 A 0 A_JumpIfCloser(250, "SecMissile7")
		VILE M 8 Bright A_FaceTarget
	Stage8:
		TNT1 A 0 A_JumpIfCloser(250, "SecMissile8")
		VILE N 8 Bright A_FaceTarget
	Final:
		VILE O 8 Bright A_VileAttack
		VILE P 20 Bright
		Goto See
	SecMissile:
		VILE G 3 Bright A_FaceTarget
	SecMissile2:
		VILE H 3 Bright A_FaceTarget
	SecMissile3:
		VILE I 3 Bright A_FaceTarget
	SecMissile4:
		VILE J 3 Bright A_FaceTarget
	SecMissile5:
		VILE K 3 Bright A_FaceTarget
	SecMissile6:
		VILE L 3 Bright A_FaceTarget
	SecMissile7:
		VILE M 3 Bright A_FaceTarget
	SecMissile8:
		VILE N 3 Bright A_FaceTarget
	SecMissileFinal:
		VILE O 3 Bright A_CustomMissile("VileFlame", 0, 0, 0, 0)
		VILE P 5 Bright
		Goto See
	Heal:
		VILE "[\]" 10 Bright
		Goto See
	Pain:
		VILE Q 5   
		VILE Q 5 A_Pain
		Goto See
	Death:
		TNT1 A 0 A_GiveToTarget("Adrenaline", 10)
		VILE Q 4
		VILE R 4 A_Scream
		VILE S 4 A_NoBlocking
		VILE TUVWXY 4
		VILE Z -1
		Stop
	XDeath:
		TNT1 A 0 A_Jump(128, "XDeath2")
	XDeath1:
		TNT1 A 0 A_GiveToTarget("Adrenaline", 12)
		VIGB A 4 A_Scream
		VIGB B 4 A_NoBlocking
		VIGB CDEFGH 4
		VIGB I -1
		Stop
	XDeath2:
		TNT1 A 0 A_GiveToTarget("Adrenaline", 12)
		VIG2 A 4 A_Scream
		VIG2 B 4 A_NoBlocking
		VIG2 CDEFGHI 4
		Stop
	}
}

Actor VileFlame {
	Radius 6
	Height 16
	Speed 20
	FastSpeed 20
	Damage 8
	Projectile
	+RANDOMIZE
	-NOGRAVITY
	+NOEXPLODEFLOOR
	+STEPMISSILE
	+ROCKETTRAIL
	Gravity 800
	MaxStepHeight 30
	MaxStepHeight 30
	RenderStyle Add
	Alpha 1
	SeeSound "baron/attack"
	DeathSound "baron/shotx"
	Decal "BaronScorch"
	States
	{
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(911)==1, "DisableSmoke")
			TNT1 A 0 A_ChangeFlag("ROCKETTRAIL", TRUE)
			Goto FirePillar
		DisableSmoke:
			TNT1 A 0 A_ChangeFlag("ROCKETTRAIL", FALSE)
		FirePillar:
			FIRE CD 4 Bright A_FireCrackle
			Loop
		Death:
			FIRE DEFGH 4 Bright
			Stop
	}
}