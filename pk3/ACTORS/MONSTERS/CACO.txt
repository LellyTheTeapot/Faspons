Actor CacoRemix : Cacodemon Replaces Cacodemon
{
	Health 400
	GibHealth 30
	Radius 31
	Height 56
	Mass 400
	Speed 8
	BloodColor "Blue"
	PainChance 64
	PainChance "SuperBuckshot", 90
	PainChance "Saw", 128
	Monster
	+FLOAT
	+NOGRAVITY
	-FRIGHTENED
	SeeSound "caco/sight"
	PainSound "caco/pain"
	DeathSound "caco/death"
	ActiveSound "caco/active"
	Obituary "$OB_CACO"
	HitObituary "$OB_CACOHIT"
	States
	{
		Spawn:
			HEAD A 10 A_Look
			Loop
		See:
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(913)==1, "SeeVanilla")
			TNT1 A 0 A_ChangeFlag("COUNTKILL", TRUE)
			TNT1 A 0 A_ChangeFlag("SLIDESONWALLS", FALSE)
			HEAD A 3 A_Chase
			Loop
		SeeVanilla:
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(913)==0, "See")
			HEAD A 3 A_Chase(0, "MissileVanilla")
			Loop
		Melee:
			TNT1 A 0 A_ChangeFlag("SLIDESONWALLS", TRUE)
			HEAD A 0 A_FaceTarget
			HEAD A 1 A_Recoil (-8)
			HEAD ABCCC 2 A_FaceTarget
			HEAD BA 2 A_CustomMeleeAttack(8, "imp/melee", NONE, 0, 0)
			HEAD A 6
			HEAD A 0 A_Stop
			Goto See
		Missile:
			TNT1 A 0 A_JumpIfCloser(125, "Melee")
			HEAD BC 5 A_FaceTarget
			HEAD D 5 Bright A_HeadAttack
			Goto See
		MissileVanilla:
			HEAD BC 5 A_FaceTarget
			HEAD D 5 Bright A_HeadAttack
			Goto See
		Dodge:
			HEAD A 0 A_Jump(128, 6)
			HEAD A 0 A_FaceTarget
			HEAD A 0 ThrustThing(angle*256/360+192, 6, 0, 0)
			HEAD E 3
			HEAD E 3 A_Pain
			HEAD F 6 A_Stop
			Goto See
			HEAD A 0 A_FaceTarget
			HEAD A 0 ThrustThing(angle*256/360+64, 6, 0, 0)
			HEAD E 3
			HEAD E 3 A_Pain
			HEAD F 6 A_Stop
			Goto See
		Pain:
			TNT1 A 0 A_JumpIf(ACS_ExecuteWithResult(913)==1, 2)
			HEAD A 0 A_Jump(192, "Dodge")
			HEAD E 3
			HEAD E 3 A_Pain
			HEAD F 6 A_Stop
			Goto See
		Death:
			TNT1 A 0 A_Jump(8, "Death.Headshot")
			TNT1 A 0 A_GiveToTarget("Adrenaline", 5)
			HEAD G 4
			HEAD H 4 A_Scream
			HEAD IJ 4
			HEAD K 4 A_NoBlocking
			HEAD L -1 A_SetFloorClip
			Stop
		Death.Plasma:
			TNT1 A 0 A_GiveInventory("PlasmaDeath", 1)
			TNT1 A 0 A_GiveToTarget("Adrenaline", 6)
			CATB A 4 A_Scream
			CATB B 4 A_NoBlocking
			CATB CDE 4
			CATB F -1 A_SetFloorClip
			Stop
		Death.Nato:
		Death.SuperBuckshot:
		Death.Buckshot:
			TNT1 A 0 A_Jump(8, "Death.Headshot")
			TNT1 A 0 A_GiveInventory("BuckDeath", 1)
			TNT1 A 0 A_GiveToTarget("Adrenaline", 5)
			CATS A 4 A_Scream
			CATS B 4 A_NoBlocking
			CATS CDEF 4
			CATS G -1 A_SetFloorClip
			Stop
		Death.Headshot:
			TNT1 A 0 A_GiveInventory("HSDeath", 1)
			TNT1 A 0 A_GiveToTarget("Adrenaline", 6)
			CAHS A 4 A_Scream
			CAHS B 4 A_NoBlocking
			CAHS CDE 4
			CAHS F -1 A_SetFloorClip
			Stop
		Death.SoulRes:
			TNT1 A 0 A_NoBlocking
			TNT1 A 0 A_JumpIfInventory("PlasmaDeath", 1, 4)
			TNT1 A 0 A_JumpIfInventory("ExDeath", 1, 4)
			TNT1 A 0 A_JumpIfInventory("BuckDeath", 1, 4)
			HEAD L -1
			Stop
			CATB F -1
			Stop
			CATE J -1
			Stop
			CATS G -1
			Stop
		XDeath:
			TNT1 A 0 A_GiveInventory("ExDeath", 1)
			TNT1 A 0 A_GiveToTarget("Adrenaline", 7)
			CATE A 4 A_Scream
			CATE B 4 A_NoBlocking
			CATE CDEFGHI 4 
			CATE J -1 A_SetFloorClip
			Stop
		Raise:
			TNT1 A 0 A_ChangeFlag("COUNTKILL", FALSE)
			TNT1 A 0 A_JumpIfInventory("PlasmaDeath", 1, "PlasRaise")
			TNT1 A 0 A_JumpIfInventory("BuckDeath", 1, "BuckRaise")
			TNT1 A 0 A_JumpIfInventory("ExDeath", 1, "ExRaise")
			TNT1 A 0 A_JumpIfInventory("HSDeath", 1, "HSRaise")
			HEAD L 1
			HEAD L 1 ACS_Execute(906)
			TNT1 A 0 A_JumpIfInventory("SoulResEnabled", 1, "SoulRaise")
			HEAD KJIHG 4
			Goto See
		PlasRaise:
			CATB F 1
			CATB F 1 ACS_Execute(906)
			TNT1 A 0 A_JumpIfInventory("SoulResEnabled", 1, "SoulRaise")
			CATB EDCBA 4 A_TakeInventory("PlasmaDeath", 1)
			Goto See
		BuckRaise:
			CATS G 1
			CATS G 1 ACS_Execute(906)
			TNT1 A 0 A_JumpIfInventory("SoulResEnabled", 1, "SoulRaise")
			CATS FEDCBA 4 A_TakeInventory("BuckDeath", 1)
			Goto See
		ExRaise:
			CATE J 1
			CATE J 1 ACS_Execute(906)
			TNT1 A 0 A_JumpIfInventory("SoulResEnabled", 1, "SoulRaise")
			CATE IHGFEDCBA 4 A_TakeInventory("ExDeath", 1)
			Goto See
		HSRaise:
			CAHS F 1
			CAHS F 1 ACS_Execute(906)
			TNT1 A 0 A_JumpIfInventory("SoulResEnabled", 1, "SoulRaise")
			CAHS EDBCA 4 A_TakeInventory("HSDeath", 1)
			Goto See
		SoulRaise:
			TNT1 A 0 ACS_Execute(908)
			TNT1 A 0 A_TakeInventory("SoulResEnabled", 1)
			HEAD L 0 A_SpawnItem("CacoSoul", 0, 0)
			HEAD L 0 A_Die("SoulRes")
			Stop
	}
}

Actor CacodemonBallRemix : CacodemonBall Replaces CacodemonBall
{
	Radius 6
	Height 8
	Speed 20
	FastSpeed 20
	Damage 5
	Projectile
	+RANDOMIZE
	RenderStyle Add
	Alpha 1
	SeeSound "caco/attack"
	DeathSound "caco/shotx"
	States
	{
	Spawn:
		BAL2 AB 4 Bright
		Loop
	Death:
		BAL2 CDE 6 Bright
		Stop
	}
}

Actor CacoSoul : CacoRemix {
	+SHADOW
	Health 40
	GibHealth 30
	Speed 5
	RenderStyle Translucent
	Alpha 0.5
	-SOLID
	+SHOOTABLE
	-COUNTKILL
	States
	{
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_JumpIfInventory("ExDeath", 1, "ExSpawn", AAPTR_MASTER)
			TNT1 A 0 A_JumpIfInventory("PlasmaDeath", 1, "PlasSpawn", AAPTR_MASTER)
			TNT1 A 0 A_JumpIfInventory("BuckDeath", 1, "BuckSpawn", AAPTR_MASTER)
			TNT1 A 0 A_JumpIfInventory("HSDeath", 1, "HSSpawn", AAPTR_MASTER)
			HEAD L 4 A_UnSetFloorClip
			HEAD KJIHG 4
			Goto Super::See
		PlasSpawn:
			CATB F 4 A_UnSetFloorClip
			CATB EDCBA 4
			Goto Super::See
		BuckSpawn:
			CATS G 4 A_UnSetFloorClip
			CATS FEDCBA 4
			Goto Super::See
		ExSpawn:
			CATE J 4 A_UnSetFloorClip
			CATE IHGFEDCBA 4
			Goto Super::See
		HSSpawn:
			CAHS F 4 A_UnSetFloorClip
			CAHS EDCBA 4
			Goto Super::See
		Death:
			HEAD G 4 A_FadeOut(0.20)
			HEAD H 4 A_Scream
			HEAD IJ 4 A_FadeOut(0.15)
			HEAD K 4 A_NoBlocking
			HEAD L -1 A_SetFloorClip
			Stop
		Death.Plasma:
			CATB A 4 A_Scream
			CATB B 4 A_NoBlocking
			CATB CDE 4 A_FadeOut(0.20)
			CATB F -1 A_SetFloorClip
			Stop
		Death.Nato:
		Death.SuperBuckshot:
		Death.Buckshot:
			CATS A 4 A_Scream
			CATS B 4 A_NoBlocking
			CATS CDEF 4 A_FadeOut(0.15)
			CATS G -1 A_SetFloorClip
			Stop
		XDeath:
			CATE A 4 A_Scream
			CATE B 4 A_NoBlocking
			CATE CDEFGHI 4 A_FadeOut(0.15)
			CATE J -1 A_SetFloorClip
			Stop
		Raise:
			Stop
	}
}