ACTOR GunSmokeSpawner
{
	Speed 20
	+NOCLIP
	States
	{
		Spawn:
			TNT1 A 1
			TNT1 A 0 Thing_ChangeTID(0,390)
			TNT1 AA 0 A_CustomMissile ("GunSmoke", 0, 0, random (0, 360), 2, random (0, 180))
			Stop
	}
}

ACTOR GunSmoke
{
	+NOGRAVITY
	+NOBLOCKMAP
	+FLOORCLIP
	+FORCEXYBILLBOARD
	+NOINTERACTION
	+FORCEXYBILLBOARD
	+MISSILE
	Speed 1
	RenderStyle Add
	Alpha		0.06
	+CLIENTSIDEONLY
	Radius		0
	Height		0
	Scale		0.6
	States
	{
		Spawn:
			TNT1 A 1
			SMKE AABBCCDDEEFFGGHHIIJJKK 1 A_FadeOut(0.001)
			Stop
	}
}

ACTOR SmokeSpawner
{
	Speed 20
	+NOCLIP
	States
	{
		Spawn:
			TNT1 A 1
			TNT1 A 0 Thing_ChangeTID(0,390)
			TNT1 A 0 A_SpawnItem("ShotSmoke",0,0)
			Stop
	}
}

ACTOR ShotSmoke
{
	+NOTDMATCH
	+NOINTERACTION
	+FORCEXYBILLBOARD
	VSpeed 1
	RenderStyle Add
	Alpha 0.3
	Scale 0.1
	States
	{
		Spawn:
			TNT1 A 0
			TNT1 A 0 Thing_ChangeTID(0,390)
			SMOK ABCDEFGHIJKLMNOPQ 1 A_SpawnItem("ShotSmokeTrail")
			Stop
	}
}

ACTOR ShotSmokeTrail : ShotSmoke
{
	VSpeed 0
	Alpha 0.15
	States
	{
		Spawn:
			SMOK CDEFGHIJKLMNOPQ 1
			Stop
	}
}

ACTOR BlackSmoke : ShotSmoke
{
	RenderStyle Translucent
	Alpha 0.15
	States
	{
		Spawn:
			TNT1 A 0
			TNT1 A 0 Thing_ChangeTID(0,390)
			SMOK ABCDEFGHIJKLMNOPQ 1 A_SpawnItem("BlackSmokeTrail") 
			SMOK Q 1 A_FadeOut(0.02)
			Wait
	}
}

ACTOR BlackSmokeTrail : ShotSmokeTrail
{
	RenderStyle Translucent
	Alpha 0.08
}

ACTOR RocketSmoke
{
	+NOBLOCKMAP
	+NOGRAVITY
	+ALLOWPARTICLES
	+RANDOMIZE
	RenderStyle Translucent
	Scale 2.0
	Alpha 0.18
	VSpeed 8
	Mass 8
	States
	{
		Spawn:
			PUF2 ABCDEFGHIJKL 1
			PUF2 MNOPQRSTUVWXYZ 2 A_FadeOut(0.002)
			Goto Death
		Death:
			PUF3 ABC 2 A_FadeOut(0.002)
		Stop
	}
}

ACTOR PlasmaSmoke
{
	+NOBLOCKMAP
	+NOGRAVITY
	+ALLOWPARTICLES
	+RANDOMIZE
	RenderStyle Translucent
	Alpha 0.15
	VSpeed 8
	Mass 5
	States
	{
		Spawn:
			SMKE ABCDEFG 2 A_FadeOut(0.001)
			Goto Death
		Death:
			SMKE HIJKLMNOPQRSTUVWXYZ 2 A_FadeOut(0.002)
			Stop
	}
}

ACTOR ShotPuff : BulletPuff
{
	Decal BulletChip
}

/*
ACTOR AltPuff : BulletPuff
{
	Decal BulletChip
	+NOEXTREMEDEATH
}
*/

ACTOR ExplosionFire
{
	Game Doom
	Radius 1
	Height 1
	Speed 4
	Damage 0 
    +NOBLOCKMAP
    +NOTELEPORT
    +DONTSPLASH
	+MISSILE
	+FORCEXYBILLBOARD
    +CLIENTSIDEONLY
    +NOINTERACTION
	+NOCLIP
    DamageType Flames
	Gravity 0
	Renderstyle Add
	States
	{
		Spawn:
			MISL BCDEFG 2 BRIGHT
			Stop
	}
}

ACTOR AltPuff : BulletPuff
{
	+NOBLOCKMAP
	+NOGRAVITY
	+ALLOWPARTICLES
	+RANDOMIZE
	+NOEXTREMEDEATH
	Decal BulletChip
	RenderStyle Translucent
	Alpha 0.7
	VSpeed 1
	Mass 5
	States
	{
		Spawn:
			PUFF A 2 Bright
			TNT1 AAAAA 0 A_CustomMissile ("PuffParticles", 0, 0, random (0, 360), 2, random (0, 360))
			TNT1 AAA 0 A_CustomMissile ("PuffSmoke", 0, 0, random (0, 360), 2, random (0, 360))
			PUFF B 2
		Melee:
			PUFF CD 2
			Stop
	}
}

ACTOR ShotgunPuff : BulletPuff
{
	+NOBLOCKMAP
	+NOGRAVITY
	+ALLOWPARTICLES
	+RANDOMIZE
	+NOEXTREMEDEATH
	Decal BulletChip
	DamageType Buckshot
	RenderStyle Translucent
	Alpha 0.7
	VSpeed 1
	Mass 5
	States
	{
		Spawn:
			PUFF A 2 Bright
			TNT1 AAAAA 0 A_CustomMissile ("PuffParticles", 0, 0, random (0, 360), 2, random (0, 360))
			TNT1 AAA 0 A_CustomMissile ("PuffSmoke", 0, 0, random (0, 360), 2, random (0, 360))
			PUFF B 2
		Melee:
			PUFF CD 2
			Stop
	}
}

ACTOR SShotgunPuff : BulletPuff
{
	+NOBLOCKMAP
	+NOGRAVITY
	+ALLOWPARTICLES
	+RANDOMIZE
	+NOEXTREMEDEATH
	Decal BulletChip
	DamageType SuperBuckshot
	RenderStyle Translucent
	Alpha 0.7
	VSpeed 1
	Mass 5
	States
	{
		Spawn:
			PUFF A 2 Bright
			TNT1 AAAAA 0 A_CustomMissile ("PuffParticles", 0, 0, random (0, 360), 2, random (0, 360))
			TNT1 AAA 0 A_CustomMissile ("PuffSmoke", 0, 0, random (0, 360), 2, random (0, 360))
			PUFF B 2
		Melee:
			PUFF CD 2
			Stop
	}
}


ACTOR NatoPuff : BulletPuff
{
	+NOBLOCKMAP
	+NOGRAVITY
	+ALLOWPARTICLES
	+RANDOMIZE
	+NOEXTREMEDEATH
	Decal BulletChip
	DamageType Nato
	RenderStyle Translucent
	Alpha 0.7
	VSpeed 1
	Mass 5
	States
	{
		Spawn:
			PUFF A 2 Bright
			TNT1 AAAAA 0 A_CustomMissile ("PuffParticles", 0, 0, random (0, 360), 2, random (0, 360))
			TNT1 AAA 0 A_CustomMissile ("PuffSmoke", 0, 0, random (0, 360), 2, random (0, 360))
			PUFF B 2
		Melee:
			PUFF CD 2
			Stop
	}
}

ACTOR ExplosionPuff : BulletPuff
{
	+NOBLOCKMAP
	+NOGRAVITY
	+ALLOWPARTICLES
	+RANDOMIZE
	Decal BulletChip
	DamageType Explosion
	RenderStyle Translucent
	Alpha 0.7
	VSpeed 1
	Mass 5
	States
	{
		Spawn:
			PUFF A 2 Bright
			TNT1 AAAAA 0 A_CustomMissile ("PuffParticles", 0, 0, random (0, 360), 2, random (0, 360))
			TNT1 AAA 0 A_CustomMissile ("PuffSmoke", 0, 0, random (0, 360), 2, random (0, 360))
			PUFF B 2
		Melee:
			PUFF CD 2
			Stop
	}
}


ACTOR PuffParticles: ExplosionFire
{
    Speed 10
	Scale 0.6
	Renderstyle Add
	States
	{
		Spawn:
			PUFF A 2 BRIGHT
			PUFF A 2
			Stop
	}
}

ACTOR PuffSmoke: ExplosionFire
{
	Speed 1
	Scale 0.9
	Renderstyle Translucent
	Alpha 0.9
	States
	{
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_Jump(128, "Spawn2")
			PUFF CCCCCCCCCCCCCCCCCC 1 A_FadeOut(0.05)
			Stop
		Spawn2:
			PUFF DDDDDDDDDDDDDDDDDD 1 A_FadeOut(0.05)
			Stop
	}
}

ACTOR CellSmoke
{
    Radius 0
    Height 0
	Alpha 0.4
	RenderStyle Translucent
	XScale 0.5
	YScale 0.2
    +NOBLOCKMAP
    +NOTELEPORT
    +DONTSPLASH
	Damage 0
    States
    {
    Spawn:
	    SB17 E 4
		SB17 ABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCD 5
		SB17 E 4
		Stop
    }
}

ACTOR BarrelSmoke
{
    Radius 0
    Height 0
	Alpha 0.4
	RenderStyle Translucent
	XScale 0.5
	YScale 0.2
    +NOBLOCKMAP
    +NOTELEPORT
    +DONTSPLASH
	Damage 0
    States
    {
    Spawn:
	    SB17 E 4
		SB17 ABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCD 5
		SB17 E 4
		Stop		
    }
}